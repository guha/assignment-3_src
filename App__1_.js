import React, { Component } from 'react';

import './App.css';
import ItemService from './item-service';

import CRUDTable,
{
  Fields,
  Field,
  CreateForm,
  UpdateForm,
  DeleteForm,
} from 'react-crud-table';

import './index.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.itemService  = new ItemService();
    this.onCreateItem = this.onCreateItem.bind(this);
    this.onUpdateItem = this.onUpdateItem.bind(this);
    this.onDeleteItem = this.onDeleteItem.bind(this);
    this.state        = {showDetails: false,
                         editItem: false,
                         selectedItem: null,
                         newItem: null
    }
  }

  componentDidMount() {
      this.getItems();
  }

  render() {
    const items = this.state.items;
    if(!items) return null;
    let tasks = this.itemService.retrieveItems();

    const DescriptionRenderer = ({ field }) => <textarea {...field} />;

    const SORTERS = {
      NUMBER_ASCENDING: mapper => (a, b) => mapper(a) - mapper(b),
      NUMBER_DESCENDING: mapper => (a, b) => mapper(b) - mapper(a),
      STRING_ASCENDING: mapper => (a, b) => mapper(a).localeCompare(mapper(b)),
      STRING_DESCENDING: mapper => (a, b) => mapper(b).localeCompare(mapper(a)),
    };

    const getSorter = (data) => {
      const mapper = x => x[data.field];
      let sorter = SORTERS.STRING_ASCENDING(mapper);

      if (data.field === 'id') {
        sorter = data.direction === 'ascending' ?
          SORTERS.NUMBER_ASCENDING(mapper) : SORTERS.NUMBER_DESCENDING(mapper);
      } else {
        sorter = data.direction === 'ascending' ?
          SORTERS.STRING_ASCENDING(mapper) : SORTERS.STRING_DESCENDING(mapper);
      }
      return sorter;
    };

    let count = tasks.length;
    const service = {
      fetchItems: (payload) => {
        this.getItems();
        let result = Array.from(this.state.items);
        result = result.sort(getSorter(payload.sort));
        return Promise.resolve(result);
      },
      create: (task) => {
        this.onCreateItem(task);
        console.log(task);
        return Promise.resolve(task);
      },
      update: (task) => {
        this.onUpdateItem(task);
        return Promise.resolve(task);
      },
      delete: (task) => {
        this.onDeleteItem(task.id);
        return Promise.resolve(task);
      },
    };

    const styles = {
      container: { margin: 'auto', width: 'fit-content' },
    };

    return (
      <div style={styles.container}>
        <CRUDTable
          caption="Tasks"
          fetchItems={payload => service.fetchItems(payload)}
        >
          <Fields>
            <Field
              name="id"
              label="Id"
            />
            <Field
              name="title"
              label="Title"
              placeholder="Title"
            />
            <Field
              name="name"
              label="Name"
              placeholder="Name"
            />
            <Field
              name="description"
              label="Description"
              render={DescriptionRenderer}
            />
          </Fields>
          <CreateForm
            title="Staff Info Creation"
            message="Add a new staff member! (Id Must be a number !!!)"
            trigger="Add Staff Member"
            onSubmit={task => service.create(task)}
            submitText="Add"
            validate={(values) => {
              const errors = {};
              if (!values.id) {
                errors.id = 'Please, provide member\'s id';
              }

              if (!values.title) {
                errors.title = 'Please, provide member\'s title';
              }

              if (!values.name) {
                errors.name = 'Please, provide member\'s name';
              }

              if (!values.description) {
                errors.description = 'Please, provide member\'s description';
              }

              return errors;
            }}
          />

          <UpdateForm
            title="Staff Info Update Process"
            message="Update staff member info"
            trigger="Update"
            onSubmit={task => service.update(task)}
            submitText="Update"
            validate={(values) => {
              const errors = {};

              if (!values.id) {
                errors.id = 'Please, provide id';
              }

              if (!values.title) {
                errors.title = 'Please, provide task\'s title';
              }

              if (!values.name) {
                errors.name = 'Please, provide member\'s name';
              }

              if (!values.description) {
                errors.description = 'Please, provide task\'s description';
              }

              return errors;
            }}
          />

          <DeleteForm
            title="Task Delete Process"
            message="Are you sure you want to delete the staff member?"
            trigger="Delete"
            onSubmit={task => service.delete(task)}
            submitText="Delete"
            validate={(values) => {
              const errors = {};

              if (!values.id) {
                errors.id = 'Please, provide id';
              }

              if (!values.title) {
                errors.title = 'Please, provide task\'s title';
              }

              if (!values.name) {
                errors.name = 'Please, provide member\'s name';
              }

              if (!values.description) {
                errors.description = 'Please, provide task\'s description';
              }
              return errors;
            }}
          />
        </CRUDTable>
      </div>
    );
  }

  getItems() {
    this.itemService.retrieveItems().then(items => {
          this.setState({items: items});
        }
    );
  }


  onUpdateItem(item) {
    this.clearState();
    this.itemService.updateItem(item).then(item => {
        this.getItems();
      }
    );
  }

  onCreateItem(newItem) {
    this.clearState();
    this.itemService.createItem(newItem).then(item => {
        this.getItems();
      }
    );
  }

  onDeleteItem(itemLink) {
    this.clearState();
    this.itemService.deleteItem(itemLink).then(res => {
        this.getItems();
      }
    );
  }

  clearState() {
    this.setState({
      showDetails: false,
      selectedItem: null,
      editItem: false,
      newItem: null
    });
  }
}

export default App;
